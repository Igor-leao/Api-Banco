## Projeto de Construção de API

Com o intuito de aplicar boas práticas e segurança de código, bons seguimentos
em semelhança ao modelo de Richardson Maturity.

Vamos nos seguintes passos

 - Construção da api
- Verificação das boas praticas em princípios Solid
- Refatoração para aplicarmos qual Design Patterns se enquadra melhor ao Codigo
- Testes
- Posível integraçaõ com Microserviços e Mensageria (preferencial Kafka)

Detalhamento da construção nos commits



## Micronaut 3.8.5 Documentation

- [User Guide](https://docs.micronaut.io/3.8.5/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.8.5/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.8.5/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
## Feature lombok documentation

- [Micronaut Project Lombok documentation](https://docs.micronaut.io/latest/guide/index.html#lombok)

- [https://projectlombok.org/features/all](https://projectlombok.org/features/all)


## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)

## API do Banco de simulacão

- [Api Cobranca de Boletos - Inter](https://developers.bancointer.com.br/reference/incluirboleto)




