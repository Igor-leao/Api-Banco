package com.api.banco.controller;

import com.api.banco.model.entity.Cobranca;
import com.api.banco.service.CobrancaService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller("api/cobranca")

public class CobrancaController {

    private final CobrancaService cobrancaService;

    public CobrancaController(CobrancaService cobrancaService) {
        this.cobrancaService = cobrancaService;
    }

    @Post
    public HttpResponse<Cobranca> createUser(@Body Cobranca cobranca) {
        return HttpResponse.created(cobrancaService.createCobranca(cobranca));
    }

    @Get
    public HttpResponse<List<Cobranca>> getAllCobrancas() {
        return HttpResponse.created(cobrancaService.getAllCobrancas());
    }

    @Get("/{cobrancaId}")
    public HttpResponse<Cobranca> getCobranca(@PathVariable UUID cobrancaId) {
        return HttpResponse.ok(cobrancaService.getCobranca(cobrancaId));
    }
}
