package com.api.banco.service;

import com.api.banco.model.entity.Cobranca;
import jakarta.inject.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Singleton

public class CobrancaService {

    private List<Cobranca> cobrancaList = new ArrayList<>();

    public Cobranca createCobranca(Cobranca cobranca) {
        cobrancaList.add(cobranca);
        return cobranca;
    }

    public List<Cobranca> getAllCobrancas() {
        return cobrancaList;
    }

    public Cobranca getCobranca(UUID cobrancaId) {
        return cobrancaList.stream()
                .filter(cobranca ->
            cobranca.getCobrancaID() == cobrancaId)
                .findFirst()
                .orElse(null);
    }
}
