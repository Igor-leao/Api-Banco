package com.api.banco.model.entity;

import io.micronaut.core.annotation.Generated;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Introspected
public class Cobranca {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private UUID cobrancaID;

    private String seuNumero;
    private Integer valorNominal;
    private String dataVencimento;

    private Integer numDiasAgenda;



}
